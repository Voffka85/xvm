package
{
    public class BOB extends Object
    {

        public static const WIDGET_BLOGGER_1:String = "#bob:widget/blogger_1";

        public static const WIDGET_BLOGGER_2:String = "#bob:widget/blogger_2";

        public static const WIDGET_BLOGGER_3:String = "#bob:widget/blogger_3";

        public static const WIDGET_BLOGGER_4:String = "#bob:widget/blogger_4";

        public static const WIDGET_BLOGGER_5:String = "#bob:widget/blogger_5";

        public static const WIDGET_BLOGGER_6:String = "#bob:widget/blogger_6";

        public static const WIDGET_BLOGGER_7:String = "#bob:widget/blogger_7";

        public static const WIDGET_BLOGGER_8:String = "#bob:widget/blogger_8";

        public static const WIDGET_BLOGGER_9:String = "#bob:widget/blogger_9";

        public static const WIDGET_BLOGGER_10:String = "#bob:widget/blogger_10";

        public static const WIDGET_BLOGGER_11:String = "#bob:widget/blogger_11";

        public static const WIDGET_BLOGGER_12:String = "#bob:widget/blogger_12";

        public static const WIDGET_SKILL_MORALE_BOOST_1:String = "#bob:widget/skill/morale_boost_1";

        public static const WIDGET_SKILL_MORALE_BOOST_2:String = "#bob:widget/skill/morale_boost_2";

        public static const WIDGET_SKILL_MORALE_BOOST_3:String = "#bob:widget/skill/morale_boost_3";

        public static const WIDGET_SKILL_MORALE_BOOST_4:String = "#bob:widget/skill/morale_boost_4";

        public static const WIDGET_SKILL_RISKY_ATTACK_1:String = "#bob:widget/skill/risky_attack_1";

        public static const WIDGET_SKILL_RISKY_ATTACK_2:String = "#bob:widget/skill/risky_attack_2";

        public static const WIDGET_SKILL_RISKY_ATTACK_3:String = "#bob:widget/skill/risky_attack_3";

        public static const WIDGET_SKILL_RISKY_ATTACK_4:String = "#bob:widget/skill/risky_attack_4";

        public static const WIDGET_SKILL_SAFE_STRATEGY:String = "#bob:widget/skill/safe_strategy";

        public static const WIDGET_SKILL_BATTLE_RUSH:String = "#bob:widget/skill/battle_rush";

        public static const WIDGET_SKILL_SAVING_BATTLE:String = "#bob:widget/skill/saving_battle";

        public static const WIDGET_SKILL_TERRITORY_CONTROL:String = "#bob:widget/skill/territory_control";

        public static const SKILL_TIMELEFT_DAYS:String = "#bob:skill/timeLeft/days";

        public static const SKILL_TIMELEFT_HOURS:String = "#bob:skill/timeLeft/hours";

        public static const SKILL_TIMELEFT_MIN:String = "#bob:skill/timeLeft/min";

        public static const SKILL_TITLE:String = "#bob:skill/title";

        public static const SKILL_DISACTIVATED:String = "#bob:skill/disactivated";

        public static const STATUS_TIMELEFT_DAYS:String = "#bob:status/timeLeft/days";

        public static const STATUS_TIMELEFT_HOURS:String = "#bob:status/timeLeft/hours";

        public static const STATUS_TIMELEFT_MIN:String = "#bob:status/timeLeft/min";

        public static const STATUS_TIMELEFT_LESSMIN:String = "#bob:status/timeLeft/lessMin";

        public static const SELECTORTOOLTIP_TITLE:String = "#bob:selectorTooltip/title";

        public static const SELECTORTOOLTIP_DESC:String = "#bob:selectorTooltip/desc";

        public static const SELECTORTOOLTIP_TIMETABLE_TITLE:String = "#bob:selectorTooltip/timeTable/title";

        public static const SELECTORTOOLTIP_TIMETABLE_TODAY:String = "#bob:selectorTooltip/timeTable/today";

        public static const SELECTORTOOLTIP_TIMETABLE_TOMORROW:String = "#bob:selectorTooltip/timeTable/tomorrow";

        public static const SELECTORTOOLTIP_TIMETABLE_EMPTY:String = "#bob:selectorTooltip/timeTable/empty";

        public static const SELECTORTOOLTIP_TILLEND:String = "#bob:selectorTooltip/tillEnd";

        public static const SELECTORTOOLTIP_AVAILABILITY_DAYS:String = "#bob:selectorTooltip/availability/days";

        public static const SELECTORTOOLTIP_AVAILABILITY_HOURS:String = "#bob:selectorTooltip/availability/hours";

        public static const SELECTORTOOLTIP_AVAILABILITY_MIN:String = "#bob:selectorTooltip/availability/min";

        public static const SELECTORTOOLTIP_AVAILABILITY_LESSMIN:String = "#bob:selectorTooltip/availability/lessMin";

        public static const CALENDARDAY_SERVERNAME:String = "#bob:calendarDay/serverName";

        public static const CALENDARDAY_TIME:String = "#bob:calendarDay/time";

        public static const PRIMETIME_TOOLTIP_SERVER_ONSERVER:String = "#bob:primeTime/tooltip/server/onServer";

        public static const PRIMETIME_TOOLTIP_SERVER_UNAVAILABLE_INTIME:String = "#bob:primeTime/tooltip/server/unavailable/inTime";

        public static const PRIMETIME_TOOLTIP_SERVER_AVAILABLE_UNTIL:String = "#bob:primeTime/tooltip/server/available/until";

        public static const ALERTMESSAGE_SOMEPERIPHERIESHALT:String = "#bob:alertMessage/somePeripheriesHalt";

        public static const ALERTMESSAGE_SINGLEMODEHALT:String = "#bob:alertMessage/singleModeHalt";

        public static const ALERTMESSAGE_ALLPERIPHERIESHALT:String = "#bob:alertMessage/allPeripheriesHalt";

        public static const ALERTMESSAGE_BUTTON:String = "#bob:alertMessage/button";

        public static const HEADERBUTTONS_FIGHTBTN_TOOLTIP_DISABLED_HEADER:String = "#bob:headerButtons/fightBtn/tooltip/disabled/header";

        public static const HEADERBUTTONS_FIGHTBTN_TOOLTIP_DISABLED_BODY:String = "#bob:headerButtons/fightBtn/tooltip/disabled/body";

        public static const HEADERBUTTONS_FIGHTBTN_TOOLTIP_CURRENTSERVERUNAVAILABLE_BODY:String = "#bob:headerButtons/fightBtn/tooltip/currentServerUnavailable/body";

        public static const HEADERBUTTONS_FIGHTBTN_TOOLTIP_VEHLEVELREQUIRED_HEADER:String = "#bob:headerButtons/fightBtn/tooltip/vehLevelRequired/header";

        public static const HEADERBUTTONS_FIGHTBTN_TOOLTIP_VEHLEVELREQUIRED_BODY:String = "#bob:headerButtons/fightBtn/tooltip/vehLevelRequired/body";

        public static const PRIMETIME_HEADER:String = "#bob:primeTime/header";

        public static const PRIMETIME_STATUS_DISABLEFIRST:String = "#bob:primeTime/status/disableFirst";

        public static const PRIMETIME_STATUS_ALLSERVERSDISABLED:String = "#bob:primeTime/status/allServersDisabled";

        public static const PRIMETIME_STATUS_UNTIL:String = "#bob:primeTime/status/until";

        public static const PRIMETIME_STATUS_PRIMEISAVAILABLE:String = "#bob:primeTime/status/primeIsAvailable";

        public static const PRIMETIME_STATUS_PRIMEWILLBEAVAILABLE:String = "#bob:primeTime/status/primeWillBeAvailable";

        public static const SYSTEMMESSAGE_NOTAVAILABLE:String = "#bob:systemMessage/notAvailable";

        public static const SYSTEMMESSAGE_SQUAD_NOTREGISTERED:String = "#bob:systemMessage/squad/notRegistered";

        public static const SYSTEMMESSAGE_PRIMETIME_BATTLESUNAVAILABLE:String = "#bob:systemMessage/primeTime/battlesUnavailable";

        public static const SYSTEMMESSAGE_PRIMETIME_BATTLESAVAILABLE:String = "#bob:systemMessage/primeTime/battlesAvailable";

        public static const ANNOUNCEMENT_TITLE:String = "#bob:announcement/title";

        public static const ANNOUNCEMENT_TITLE_NA_ASIA:String = "#bob:announcement/title/na_asia";

        public static const ANNOUNCEMENT_REGISTRATION_HEADER:String = "#bob:announcement/registration/header";

        public static const ANNOUNCEMENT_EVENTSTART_HEADER:String = "#bob:announcement/eventStart/header";

        public static const ANNOUNCEMENT_PRIMETIME_HEADER:String = "#bob:announcement/primeTime/header";

        public static const ANNOUNCEMENT_EVENTFINISH_HEADER:String = "#bob:announcement/eventFinish/header";

        public static const ANNOUNCEMENT_EVENTFINISH_DESCRIPTION:String = "#bob:announcement/eventFinish/description";

        public static const ANNOUNCEMENT_LASTBATTLESSCORE_HEADER:String = "#bob:announcement/lastBattlesScore/header";

        public static const ANNOUNCEMENT_LASTBATTLESSCORE_DESCRIPTION:String = "#bob:announcement/lastBattlesScore/description";

        public static const ANNOUNCEMENT_AVAILABLEREWARDS_HEADER:String = "#bob:announcement/availableRewards/header";

        public static const ANNOUNCEMENT_AVAILABLEREWARDS_DESCRIPTION:String = "#bob:announcement/availableRewards/description";

        public static const ANNOUNCEMENT_TEAMLEVELUP_HEADER:String = "#bob:announcement/teamLevelUp/header";

        public static const ANNOUNCEMENT_TEAMLEVELUP_DESCRIPTION:String = "#bob:announcement/teamLevelUp/description";

        public static const ANNOUNCEMENT_SKILLACTIVATED_HEADER:String = "#bob:announcement/skillActivated/header";

        public static const ANNOUNCEMENT_SKILLACTIVATED_DESCRIPTION:String = "#bob:announcement/skillActivated/description";

        public static const ANNOUNCEMENT_SELECTTEARX_HEADER:String = "#bob:announcement/selectTearX/header";

        public static const ANNOUNCEMENT_TILLDATE_DESCRIPTION:String = "#bob:announcement/tillDate/description";

        public static const BATTLELOADING_TEAM_HEADER:String = "#bob:battleLoading/team/header";

        public static const BATTLE_TEAM_HEADER:String = "#bob:battle/team/header";

        public static const REWARDSSCREEN_TITLE:String = "#bob:rewardsScreen/title";

        public static const REWARDSSCREEN_RENTTITLE:String = "#bob:rewardsScreen/rentTitle";

        public static const REWARDSSCREEN_OPENNEXT:String = "#bob:rewardsScreen/openNext";

        public static const REWARDSSCREEN_PERSONAL:String = "#bob:rewardsScreen/personal";

        public static const REWARDSSCREEN_PERSONAL_NA_ASIA:String = "#bob:rewardsScreen/personal/na_asia";

        public static const REWARDSSCREEN_PERSONAL_BACKBTN:String = "#bob:rewardsScreen/personal/backBtn";

        public static const REWARDSSCREEN_TEAM_BACKBTN:String = "#bob:rewardsScreen/team/backBtn";

        public static const REWARDSSCREEN_TEAM_VEHICLE:String = "#bob:rewardsScreen/team/vehicle";

        public static const REWARDSSCREEN_TEAM_CAMOUFLAGE:String = "#bob:rewardsScreen/team/camouflage";

        public static const REWARDSSCREEN_TEAM_ACHIEVEMENT:String = "#bob:rewardsScreen/team/achievement";

        public static const QUESTS_BONUSES_TOKEN_BOB_POINTS_HEADER:String = "#bob:quests/bonuses/token/bob_points/header";

        public static const QUESTS_BONUSES_TOKEN_BOB_POINTS_BODY:String = "#bob:quests/bonuses/token/bob_points/body";

        public static const QUESTS_BONUSES_TOKEN_BOB_POINTS_NA_ASIA_HEADER:String = "#bob:quests/bonuses/token/bob_points/na_asia/header";

        public static const QUESTS_BONUSES_TOKEN_BOB_POINTS_NA_ASIA_BODY:String = "#bob:quests/bonuses/token/bob_points/na_asia/body";

        public static const SERVICECHANNELMESSAGES_REWARD:String = "#bob:serviceChannelMessages/reward";

        public static const WIDGET_ENUM:Array = [WIDGET_BLOGGER_1,WIDGET_BLOGGER_2,WIDGET_BLOGGER_3,WIDGET_BLOGGER_4,WIDGET_BLOGGER_5,WIDGET_BLOGGER_6,WIDGET_BLOGGER_7,WIDGET_BLOGGER_8,WIDGET_BLOGGER_9,WIDGET_BLOGGER_10,WIDGET_BLOGGER_11,WIDGET_BLOGGER_12,WIDGET_SKILL_MORALE_BOOST_1,WIDGET_SKILL_MORALE_BOOST_2,WIDGET_SKILL_MORALE_BOOST_3,WIDGET_SKILL_MORALE_BOOST_4,WIDGET_SKILL_RISKY_ATTACK_1,WIDGET_SKILL_RISKY_ATTACK_2,WIDGET_SKILL_RISKY_ATTACK_3,WIDGET_SKILL_RISKY_ATTACK_4,WIDGET_SKILL_SAFE_STRATEGY,WIDGET_SKILL_BATTLE_RUSH,WIDGET_SKILL_SAVING_BATTLE,WIDGET_SKILL_TERRITORY_CONTROL];

        public function BOB()
        {
            super();
        }

        public static function widget(param1:String) : String
        {
            var _loc2_:String = "#bob:" + "widget/" + param1;
            if(WIDGET_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[widget]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }
    }
}
