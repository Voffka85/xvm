package net.wg.data.constants.generated
{
    public class BATTLE_OF_BLOGGERS_ALIASES extends Object
    {

        public static const WIDGET_COMPONENT:String = "bobWidgetComponent";

        public static const BOB_PRIME_TIME_ALIAS:String = "bobPrimeTime";

        public function BATTLE_OF_BLOGGERS_ALIASES()
        {
            super();
        }
    }
}
