package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IRegularItemsWithTypeFilterTabViewMeta extends IEventDispatcher
    {

        function navigateToStoreS() : void;
    }
}
