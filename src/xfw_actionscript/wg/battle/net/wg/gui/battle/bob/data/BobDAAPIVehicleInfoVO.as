package net.wg.gui.battle.bob.data
{
    import net.wg.data.VO.daapi.DAAPIVehicleInfoVO;

    public class BobDAAPIVehicleInfoVO extends DAAPIVehicleInfoVO
    {

        public var bloggerID:int;

        public var isBlogger:Boolean;

        public function BobDAAPIVehicleInfoVO(param1:Object = null)
        {
            super(param1);
        }
    }
}
