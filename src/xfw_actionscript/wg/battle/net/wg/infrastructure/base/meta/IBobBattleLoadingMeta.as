package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IBobBattleLoadingMeta extends IEventDispatcher
    {

        function as_setBloggerIds(param1:Number, param2:Number) : void;
    }
}
